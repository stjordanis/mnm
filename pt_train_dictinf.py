from __future__ import print_function

import argparse
import random
import numpy as np

from ptTask import DictInf, DoubleCopyTask, AddTask

import torch.optim as optim
from torch.autograd import Variable
import torch
import torch.nn as nn
import torch.nn.functional as F
from random import randint

from sklearn.metrics import accuracy_score

from ptLstmAtt import ptLstmAtt
from MNMg import MNMg
from MNMp import MNMp

class MNMpSeq2Seq(nn.Module):

    def __init__(self, n_vocab, n_units, n_batch_mem = 1):
        super(MNMpSeq2Seq, self).__init__()

        self.embed_idx = nn.Embedding(n_vocab, n_units)
        self.mnm = MNMp(n_units, n_in_mem = n_units, n_units_mem = 100, n_batch_mem = n_batch_mem)
        self.w_out = nn.Linear(n_units, n_vocab)
        
        self.n_vocab = n_vocab
        self.n_units = n_units
        
    def reset_state(self):
        self.mnm.reset_state()
        
    def forward(self, input_x, trg):
        self.reset_state()

        input_x = torch.cuda.LongTensor(input_x)
        input_x = self.embed_idx(input_x)
        input_x = torch.chunk(input_x, input_x.shape[1], dim=1)

        ys_all = []
        const_loss_all = 0
        re_const_loss_init_all = 0
        for x in input_x:
            x = x.squeeze(1)
            h, const_loss, re_const_loss_init = self.mnm(x)
            const_loss_all += const_loss
            re_const_loss_init_all += re_const_loss_init
            ys_all.append(h.unsqueeze(1))

        const_loss_all /= len(input_x)
        re_const_loss_init_all /= len(input_x)

        trg_len = trg.shape[1]
        y = ys_all[-trg_len:]
        y = torch.cat(y, dim=1)
        y = self.w_out(y)
        y = torch.transpose(y, 1, 2)
        y_preds = y.argmax(1).cpu().numpy()

        trg = torch.cuda.LongTensor(trg)
        loss = F.cross_entropy(y, trg)
        
        return y_preds, loss, const_loss_all, re_const_loss_init_all

    def train(self, x, y):

        y_preds, loss, const_loss_all, re_const_loss_init_all = self.forward(x, y)

        return y_preds, loss, const_loss_all, re_const_loss_init_all


class MNMgSeq2Seq(nn.Module):

    def __init__(self, n_vocab, n_units, n_batch_mem = 1):
        super(MNMgSeq2Seq, self).__init__()

        self.embed_idx = nn.Embedding(n_vocab, n_units)
        self.mnm = MNMg(n_units, n_in_mem = n_units, n_units_mem = 100, n_batch_mem = n_batch_mem)
        self.w_out = nn.Linear(n_units, n_vocab)
        
        self.n_vocab = n_vocab
        self.n_units = n_units
        
    def reset_state(self):
        self.mnm.reset_state()
        
    def forward(self, input_x, trg):
        self.reset_state()

        input_x = torch.cuda.LongTensor(input_x)
        input_x = self.embed_idx(input_x)
        input_x = torch.chunk(input_x, input_x.shape[1], dim=1)

        ys_all = []
        const_loss_all = 0
        re_const_loss_init_all = 0
        for x in input_x:
            x = x.squeeze(1)
            h, const_loss, re_const_loss_init = self.mnm(x)
            const_loss_all += const_loss
            re_const_loss_init_all += re_const_loss_init
            ys_all.append(h.unsqueeze(1))

        const_loss_all /= len(input_x)
        re_const_loss_init_all /= len(input_x)

        trg_len = trg.shape[1]
        y = ys_all[-trg_len:]
        y = torch.cat(y, dim=1)
        y = self.w_out(y)
        y = torch.transpose(y, 1, 2)
        y_preds = y.argmax(1).cpu().numpy()

        trg = torch.cuda.LongTensor(trg)
        loss = F.cross_entropy(y, trg)
        
        return y_preds, loss, const_loss_all, re_const_loss_init_all

    def train(self, x, y):

        y_preds, loss, const_loss_all, re_const_loss_init_all = self.forward(x, y)

        return y_preds, loss, const_loss_all, re_const_loss_init_all

class AttSeq2Seq(nn.Module):

    def __init__(self, n_vocab, n_units, n_batch_mem = 1):
        super(AttSeq2Seq, self).__init__()

        self.embed_idx = nn.Embedding(n_vocab, n_units)
        self.lstmatt = ptLstmAtt(n_units, n_in_mem = n_units, n_units_mem = 100, n_batch_mem = 1)
        self.w_out = nn.Linear(n_units, n_vocab)
        
        self.n_vocab = n_vocab
        self.n_units = n_units
        
    def reset_state(self):
        self.lstmatt.reset_state()
        self.zero = torch.zeros(()).cuda().float()
        
    def forward(self, input_x, trg):
        self.reset_state()

        input_x = torch.cuda.LongTensor(input_x)
        input_x = self.embed_idx(input_x)
        input_x = torch.chunk(input_x, input_x.shape[1], dim=1)

        ys_all = []
        for x in input_x:
            x = x.squeeze(1)
            h = self.lstmatt(x)
            ys_all.append(h.unsqueeze(1))

        trg_len = trg.shape[1]
        y = ys_all[-trg_len:]
        y = torch.cat(y, dim=1)
        y = self.w_out(y)
        y = torch.transpose(y, 1, 2)
        y_preds = y.argmax(1).cpu().numpy()

        trg = torch.cuda.LongTensor(trg)
        loss = F.cross_entropy(y, trg)
        
        return y_preds, loss

    def train(self, x, y):

        y_preds, loss = self.forward(x, y)

        return y_preds, loss, self.zero, self.zero

class LSTMSeq2Seq(nn.Module):

    def __init__(self, n_vocab, n_units, n_batch_mem = 1):
        super(LSTMSeq2Seq, self).__init__()

        self.embed_idx = nn.Embedding(n_vocab, n_units)
        self.lstm = nn.LSTMCell(n_units, n_units)
        self.w_out = nn.Linear(n_units, n_vocab)
        
        self.n_vocab = n_vocab
        self.n_units = n_units
        
    def reset_state(self):
        self.zero = torch.zeros(()).cuda().float()

        self.h_lstm = None
        self.c_lstm = None
        
    def forward(self, input_x, trg):
        self.reset_state()

        
        input_x = torch.cuda.LongTensor(input_x)
        input_x = self.embed_idx(input_x)
        input_x = torch.chunk(input_x, input_x.shape[1], dim=1)

        self.h_lstm = torch.zeros((input_x[0].shape[0], self.n_units)).cuda().float()
        self.c_lstm = torch.zeros((input_x[0].shape[0], self.n_units)).cuda().float()

        ys_all = []
        for x in input_x:
            x = x.squeeze(1)
            self.h_lstm, self.c_lstm = self.lstm(x, (self.h_lstm, self.c_lstm))
            ys_all.append(self.h_lstm.unsqueeze(1))

        trg_len = trg.shape[1]
        y = ys_all[-trg_len:]
        y = torch.cat(y, dim=1)
        y = self.w_out(y)
        y = torch.transpose(y, 1, 2)
        y_preds = y.argmax(1).cpu().numpy()

        trg = torch.cuda.LongTensor(trg)
        loss = F.cross_entropy(y, trg)
        
        return y_preds, loss

    def train(self, x, y):

        y_preds, loss = self.forward(x, y)

        return y_preds, loss, self.zero, self.zero

def main():
    parser = argparse.ArgumentParser(description='Pytorch example: MNM models')
    parser.add_argument('--gpu', '-g', type=int, default=0,
                        help='GPU ID')
    parser.add_argument("--prefix", help="prefix", default='none')
    parser.add_argument('--mb', '-n', type=int, default=1,
                        help='mem batch_size')
    parser.add_argument("--task", help="dictinf | dcopy | add", default='dictinf')
    parser.add_argument("--model", help="mnmg | mnmp | lstm | salu", default='mnmg')
    parser.add_argument("--npairs", type=int, default=4, help="2, 4, 8, 16 etc.")
    parser.add_argument("--nitems", type=int, default=2, help="2, 4, 8, 16 etc.")
    parser.add_argument("--checkpoint", type=int, default=1000)
    args = parser.parse_args()
    print (args)
    prefix = args.prefix + '_model_' + args.model + '_task_' + args.task + '_mb_' + str(args.mb) + '_npairs' + str(args.npairs) + '_nitems' + str(args.nitems)

    rand_seed = randint(0, 100000000)#1234 
    print ("rand_seed:", rand_seed)
    np.random.seed(rand_seed); random.seed(rand_seed); torch.manual_seed(rand_seed)

    batch_size = 32
    print ('batch_size:', batch_size)

    if args.model == 'mnmg':
        if args.task == 'dictinf':
            train_generator = DictInf(batch_size=batch_size, max_iter=1000000, size=6, min_num_items=args.npairs, max_num_items=args.npairs, \
                min_item_length=args.nitems, max_item_length=args.nitems, checkpoint=args.checkpoint)
            model = MNMgSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        elif args.task == 'dcopy':
            train_generator = DoubleCopyTask(batch_size=batch_size, max_iter=1000000, size=8, min_length=50, max_length=50, checkpoint=args.checkpoint)
            model = MNMgSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        elif args.task == 'add':
            train_generator = AddTask(batch_size=batch_size, max_iter=1000000, size=8, min_length=50, max_length=50, checkpoint=100)
            model = MNMgSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        else:
            raise ValueError("task argument is not dictinf | dcopy | add")
    if args.model == 'mnmp':
        if args.task == 'dictinf':
            train_generator = DictInf(batch_size=batch_size, max_iter=1000000, size=6, min_num_items=args.npairs, max_num_items=args.npairs, \
                min_item_length=args.nitems, max_item_length=args.nitems, checkpoint=args.checkpoint)
            model = MNMpSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)

        elif args.task == 'dcopy':
            train_generator = DoubleCopyTask(batch_size=batch_size, max_iter=1000000, size=8, min_length=50, max_length=50, checkpoint=args.checkpoint)
            model = MNMpSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        elif args.task == 'add':
            train_generator = AddTask(batch_size=batch_size, max_iter=1000000, size=8, min_length=50, max_length=50, checkpoint=100)
            model = MNMpSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        else:
            raise ValueError("task argument is not dictinf | dcopy | add")
    elif args.model == 'lstm':
        if args.task == 'dictinf':
            train_generator = DictInf(batch_size=batch_size, max_iter=1000000, size=6, min_num_items=args.npairs, max_num_items=args.npairs, \
                min_item_length=args.nitems, max_item_length=args.nitems, checkpoint=args.checkpoint)
            model = LSTMSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        elif args.task == 'dcopy':
            train_generator = DoubleCopyTask(batch_size=batch_size, max_iter=1000000, size=8, min_length=50, max_length=50, checkpoint=args.checkpoint)
            model = LSTMSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        elif args.task == 'add':
            train_generator = AddTask(batch_size=batch_size, max_iter=1000000, size=8, min_length=50, max_length=50, checkpoint=100)
            model = LSTMSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        else:
            raise ValueError("task argument is not dictinf | dcopy | add")
    elif args.model == 'salu':
        if args.task == 'dictinf':
            train_generator = DictInf(batch_size=batch_size, max_iter=1000000, size=6, min_num_items=args.npairs, max_num_items=args.npairs, \
                min_item_length=args.nitems, max_item_length=args.nitems, checkpoint=args.checkpoint)
            model = AttSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        elif args.task == 'dcopy':
            train_generator = DoubleCopyTask(batch_size=batch_size, max_iter=1000000, size=8, min_length=50, max_length=50, checkpoint=args.checkpoint)
            model = AttSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        elif args.task == 'add':
            train_generator = AddTask(batch_size=batch_size, max_iter=1000000, size=8, min_length=50, max_length=50, checkpoint=100)
            model = AttSeq2Seq(n_vocab=train_generator.n_vocab, n_units=100, n_batch_mem=args.mb)
        else:
            raise ValueError("task argument is not dictinf | dcopy | add")

    model = model.to(args.gpu)
    
    params = []
    for param in model.parameters():
        params.append(param)

    optimizer = optim.Adam(params)
    
    total_loss = 0
    total_const_loss = 0
    re_const_loss_init_all = 0
    total_acc = 0
    
    loss_all_list = []
    acc_list = []

    while train_generator.cur_iter <= 100000:
        batch = train_generator.next()
        x = batch[0]
        y = batch[1]

        y_preds, loss, const_loss, re_const_loss_init = model.train(x, y)


        # this is used for sparse update to monitor memory capacity
        # if args.model == 'mnmg':
        #     mem_usage_all += model.predictor.auto_lstm.auto_mem.current_mem_usage()
        y_true = y.reshape((-1, ))
        y_preds = y_preds.reshape((-1, ))
        acc = accuracy_score(y_true, y_preds)
        acc_list.append(acc)

        model.zero_grad()
        loss_t = loss + const_loss
        loss_t.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 10)
        optimizer.step()
        
        
        total_loss += loss.float().item()
        total_const_loss += const_loss.float().item()
        re_const_loss_init_all += re_const_loss_init.float().item()
        total_acc += acc
        
        loss_all_list.append(loss.float().item())

        if train_generator.cur_iter % train_generator.checkpoint == 0 and train_generator.cur_iter>0:
            
            print ("iter: ", train_generator.cur_iter)
            print ("train acc: ", total_acc/train_generator.checkpoint)
            print ("train loss: ", total_loss/train_generator.checkpoint)
            print ("train memory reconst_loss (pre-update):", re_const_loss_init_all/train_generator.checkpoint)
            print ("train memory reconst_loss (post-update):",  total_const_loss/train_generator.checkpoint)
            print ("memory reconst_loss gain:", (re_const_loss_init_all - total_const_loss)/train_generator.checkpoint)

            total_loss = 0
            total_const_loss = 0
            re_const_loss_init_all = 0
            total_acc = 0

if __name__ == '__main__':
    main()