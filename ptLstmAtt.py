import time
import numpy as np

from torch.autograd import Variable
import torch
import torch.nn as nn
import torch.nn.functional as F

class ptLstmAtt(nn.Module):

	def __init__(self, n_units, n_in_mem = 50, n_units_mem = 150, n_batch_mem=1):
		if n_in_mem is None:
			n_in_mem = n_units
		if n_units_mem is None:
			n_units_mem = n_units
		super(ptLstmAtt, self).__init__()

		self.lstm_l1 = nn.LSTMCell(n_units+n_in_mem, n_units)
		self.heads_l2 = nn.Linear(n_units, (n_batch_mem*3)*n_in_mem)
		self.read_out = nn.Linear(n_units+n_in_mem, n_units)
		

		self.n_batch_mem = n_batch_mem
		self.n_in_mem = n_in_mem
		self.n_units_mem = n_units_mem
		self.n_units = n_units
		self.h = None
		self.replay_k = []
		self.replay_v = []

	def unchain_state(self):
		if self.h is not None:
			self.h = self.h.detach()
			self.h_lstm = self.h_lstm.detach()
			self.c_lstm = self.c_lstm.detach()

		self.replay_k[:] = []
		self.replay_v[:] = []

	def reset_state(self):
		self.h = None
		self.replay_k[:] = []
		self.replay_v[:] = []

		self.h_lstm = None
		self.c_lstm = None

	def forward(self, x):
		if self.h is None:
			self.h = torch.zeros((x.shape[0], self.n_in_mem)).cuda().float()
			self.h_lstm = torch.zeros((x.shape[0], self.n_units)).cuda().float()
			self.c_lstm = torch.zeros((x.shape[0], self.n_units)).cuda().float()

		self.h_lstm, self.c_lstm = self.lstm_l1(torch.cat([x, self.h], dim=1), (self.h_lstm, self.c_lstm))
		h = self.heads_l2(self.h_lstm)
		
		n_k_v = h.view(h.shape[0], 1, -1).contiguous()
		k_w, v_w, k_r = torch.chunk(n_k_v, 3, dim=2)

		self.replay_k.append(k_w)
		self.replay_v.append(v_w)

		
		k_mem = torch.cat(self.replay_k, dim=1)
		v_mem = torch.cat(self.replay_v, dim=1)

		a = torch.matmul(k_mem, k_r.transpose(1, 2))
		a = a.view(a.shape[0], -1)
		
		self.h = torch.matmul(torch.softmax(a, dim=1).unsqueeze(2).transpose(1,2), v_mem)
		self.h = self.h.view(self.h.shape[0], -1)
		
		h_lstm = self.read_out(torch.cat([self.h_lstm, self.h], dim=1))

		return h_lstm